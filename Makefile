


LIQUIBASE = liquibase


lq_cmd = $(LIQUIBASE) --url '$(LIQUIBASE_URL)'   --password $(LIQUIBASE_PASSWORD)  --username $(LIQUIBASE_USERNAME)


.PHONY: update


update:
	$(lq_cmd) update

