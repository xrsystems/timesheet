--liquibase formatted sql
--changeset christian.muehlhaus:1-init-create-table-users

CREATE TABLE users
(
   id           number,             
   row_version  number NOT NULL,
   created_by   number NOT NULL,
   created_on   timestamp with local time zone NOT NULL ,
   updated_by   number NOT NULL ,
   updated_on   timestamp with local time zone NOT NULL ,
 
   CONSTRAINT users_PK  PRIMARY KEY (id)
);
COMMENT ON TABLE users IS 'User accounts';

--changeset christian.muehlhaus:2-add-username-column
ALTER TABLE users ADD (username VARCHAR2(200 CHAR)); 
ALTER TABLE users ADD CONSTRAINT users_uk UNIQUE (username);