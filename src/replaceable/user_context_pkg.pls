create or replace package user_context_pkg is
  /*  
     
     %version 1
     %author  CMUEHLHAUS
     
     %note  CREATE CONTEXT TIMESHEET_CTX USING timesheet_dev.user_context_pkg;

  */
   
   c_context_name CONSTANT varchar2(30 byte) := 'TIMESHEET_CTX';
   
   
   PROCEDURE set_context(p_varname varchar2, p_value varchar2); 
      
   FUNCTION get_context(p_varname varchar2) RETURN varchar2; 
    
  
   PROCEDURE set_current_username(p_username varchar2 );
   
   PROCEDURE set_current_user_id(p_user_id IN number);  
   
   FUNCTION get_current_username RETURN varchar2;
   
   FUNCTION get_current_user_id RETURN number; 

   


end user_context_pkg;
/
create or replace package body user_context_pkg is

    v_context_client_id varchar2(64) := null;
    
 /*
   */
   PROCEDURE set_context(p_varname varchar2, p_value varchar2) AS
   BEGIN
      dbms_session.set_context(namespace => c_context_name, attribute => p_varname, VALUE => p_value,
                               client_id => v_context_client_id);
   END set_context;

   /*
   */
   FUNCTION get_context(p_varname varchar2) RETURN varchar2 IS
   BEGIN
      RETURN sys_context(c_context_name, p_varname);
   END;



   PROCEDURE set_current_username(p_username varchar2 ) IS
      v_user_id number; 
   BEGIN
      SELECT id INTO v_user_id FROM users WHERE username = p_username; 
      
      set_current_user_id(p_user_id => v_user_id); 
   END; 
   
   
   
   
   PROCEDURE set_current_user_id(p_user_id IN number) IS
      v_username users.username%type; 
   BEGIN
      SELECT username  INTO v_username FROM users WHERE id = p_user_id; 
      
      set_context('USER_ID', p_user_id); 
      set_context('USERNAME', v_username);
   END;  
   
   
   
   FUNCTION get_current_username RETURN varchar2 IS 
      v_user_id number; 
   BEGIN
      return get_context('USERNAME');      
   END; 
   
   FUNCTION get_current_user_id RETURN number IS 
   BEGIN
      return get_context('USER_ID'); 
   END; 

end user_context_pkg;
/
